import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { apiURL } from '@/helpers/config';
import pathField from '@/helpers/pathField';

import corsDisabled from '@/helpers/corsDisabled'

Vue.use(Vuex);

const store = () => new Vuex.Store({
    getters: {
        getUser: state => {
            return state.user;
        },
        getCoupon: state => {
            return state.coupon;
        },
        getRewardInfo: state => {
            return state.rewardInfo;
        },
        getRewardCoupon: state => {
            return state.rewardCoupon.coupons;
        },
        getShippingId: state => {
            return state.shippingId;
        },
        getReviews: state => {
            return state.reviews;
        },
        getFiltersReview: state => {
            return state.filtersReview;
        },
        getOrder: state => {
            return state.order;
        },
        getOrderProducts: state => {
            return state.orderProducts;
        }
    },
    state: {
        user: null,
        coupon: null,
        rewardInfo: {},
        rewardCoupon: {},
        shippingId: null,
        reviews: {},
        filtersReview: {},
        order: {
            shipping: {},
            price: {},
            payment: {}
        },
        orderProducts: [],
        windowWidth: 0,
        scrolled: 0,
        showFullPageMenu: true
    },
    mutations: {
        SET_DATA(state, { field, data }) {
            pathField(state, field, data)
        },
        setUser(state, user) {
            state.user = user;
        },
        setCoupon(state, coupon) {
            state.coupon = coupon;
        },
        setRewardInfo(state, reward) {
            state.rewardInfo = reward;
        },
        setRewardCoupon(state, coupon) {
            state.rewardCoupon = coupon;
        },
        clearCoupon(state) {
            state.coupon = null;
        },
        setShippingId(state, id) {
            state.shippingId = id;
        },
        setReviewsList(state, reviews) {
            let reviewList = reviews;
            reviewList.reviews.forEach(element => {
                let s = element.created_at;
                let a = s.split(/[^0-9]/);
                let date = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
                let hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
                let h = date.getHours();
                let beforeMin = date.getMinutes() < 10 ? '0' : '';
                let beforeHours = hours < 10 ? '0' : '';
                let beforeMonth = date.getMonth() < 10 ? '0' : '';
                let ampm = h > 12 ? 'PM' : 'AM';
                let dateString = `${beforeMonth + date.getMonth()}/${date.getDate()}/${date.getFullYear()}
                          ${beforeHours + hours}:${beforeMin + date.getMinutes()} ${ampm}`;
                element.created_at = dateString;
            });
            state.reviews = reviewList;
        },
        setReviewEdit(state, review) {
            let reviews = state.reviews.reviews;
            reviews = reviews.map(item => item.id === review.id ? review : item);
            state.reviews.reviews = reviews;
        },
        setDeleteReview(state, reviewId) {
            let reviews = state.reviews.reviews;
            let i = reviews.map(item => item.id).indexOf(reviewId);
            reviews.splice(i, 1);
        },
        setReviewFilter(state, filters) {
            state.filtersReview = filters;
        },
        setOrderDetails(state, order) {
            state.order = {};
            let res = order;

            let s = res.createdAt;
            let a = s.split(/[^0-9]/);
            let date = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
            let monthArr = ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'June', 'July', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];

            state.order = {
                shipping: {
                    name: res.customer,
                    address: res.address,
                    country: res.country,
                    city: res.city,
                    state: res.state,
                    zip: res.zip,
                    phone: res.phone
                },
                price: {
                    shipping: Number.prototype.toFixed.call(parseFloat(res.shipping_cost) || 0, 2),
                    subtotal: Number.prototype.toFixed.call(parseFloat(res.subtotal_cost) || 0, 2), //res.subscription_total_cost 
                    subscriptionTotal: Number.prototype.toFixed.call(parseFloat(res.subscription_total_cost) || 0, 2),
                    discount: Number.prototype.toFixed.call(parseFloat(res.discount_cost) || 0, 2),
                    total: Number.prototype.toFixed.call(parseFloat(res.total_cost) || 0, 2),
                },
                payment: {
                    payType: res.payment_method ? res.payment_method.type : null,
                    date: res.payment_method ? monthArr[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear() : null,
                    type: res.payment_method ? res.payment_method.details.card_type : null,
                    info: res.payment_method ? res.payment_method.details.last_four : null,
                    email: res.payment_method ? res.payment_method.details.email : null
                }
            };

            state.orderProducts = []

            if (res.orderDetailed && res.orderDetailed.length > 0) {
                res.orderDetailed.forEach(item => {
                    let data = {};
                    data.id = item.id;
                    data.productId = item.product_id;
                    data.slug = item.slug;
                    data.name = item.name;
                    data.quantity = item.quantity;
                    data.price = Number.prototype.toFixed.call(parseFloat(item.price) || 0, 2);
                    data.delivery_period = false;
                    data.active = item.active;
                    data.image = (item.image) ? apiURL + item.image[0].media.filename : '';
                    data.pdfs = item && item.pdfs;

                    state.orderProducts.push(data);
                });
            }
            if (res.subscriptions && res.subscriptions.length > 0) {
                res.subscriptions.forEach(item => {
                    let data = {};
                    data.id = item.id;
                    data.productId = item.plan.delivery_period.product.id;
                    data.name = item.product;
                    data.image = (item.image) ? apiURL + item.image[0].media.filename : '';
                    data.quantity = false;
                    data.delivery_period = true;
                    data.active = item.active;
                    data.price = Number.prototype.toFixed.call(parseFloat(item.purchase_price) || 0, 2);
                    data.pdfs = item && item.pdfs;

                    state.orderProducts.push(data);
                });
            }
        }
    },
    actions: {
        getUser({
            commit
        }, token) {
            return axios({
                method: 'get',
                url: corsDisabled(apiURL + '/api/user'),
                headers: {
                    Authorization: 'Bearer ' + token,
                },
            })
                .then(response => {
                    commit('setUser', response.data);
                    return response.data;
                })
                .catch(error => {
                    commit('setUser', null)
                });
        },
        getUserCoupon({
            commit
        }, data) {
            return axios({
                method: 'post',
                url: apiURL + '/api/user/cart/' + data.cartToken + '/apply-coupon',
                headers: {
                    Authorization: 'Bearer ' + data.token,
                },
                data: {
                    code: data.coupon
                }
            })
                .then(response => {
                    commit('setCoupon', { discount: response.data.discount, code: data.coupon });
                    return data.coupon;
                })
                .catch(error => {
                    commit('setCoupon', null)
                });
        },
        getPublicCoupon({
            commit
        }, data) {
            return axios({
                method: 'post',
                url: apiURL + '/api/public/cart/' + data.cartToken + '/apply-coupon',
                data: {
                    code: data.coupon
                }
            })
                .then(response => {
                    commit('setCoupon', { discount: response.data.discount, code: data.coupon });
                    return data.coupon;
                })
                .catch(error => {
                    commit('setCoupon', null)
                });
        },
        getDiscount({
            commit
        }, data) {
            return axios({
                method: 'get',
                url: `${apiURL}/api/public/cart/${data.cartToken}/discount`
            })
                .then(response => {
                    commit('setCoupon', { discount: response.data.discount });
                })
                .catch(error => {
                    commit('setCoupon', null)
                });
        },
        getRewardInfo({
            commit
        }, token) {
            return axios({
                method: 'get',
                url: apiURL + '/api/user/reward-information',
                headers: {
                    Authorization: 'Bearer ' + token,
                }
            })
                .then(response => {
                    commit('setRewardInfo', response.data);
                })
                .catch(error => {

                });
        },
        getRewardCoupon({
            commit
        }, token) {
            return axios({
                method: 'get',
                url: apiURL + '/api/user/coupons',
                headers: {
                    Authorization: 'Bearer ' + token,
                }
            })
                .then(response => {
                    commit('setRewardCoupon', response.data);
                })
        },
        createReview({
            commit
        }, review) {
            return axios({
                method: 'post',
                url: apiURL + '/api/review/' + review.productId,
                headers: {
                    Authorization: 'Bearer ' + review.token,
                },
                data: {
                    title: review.title,
                    description: review.description,
                    rating: review.rating
                }
            })
        },
        getReviewsList({
            commit
        }, data) {
            console.log('data', data)
            return axios({
                method: 'get',
                url: `${apiURL}/api/admin/reviews?page=${data.page}&limit=${data.limit}&filter=${data.filter}`,
                headers: {
                    Authorization: 'Bearer ' + data.token
                }
            })
                .then(res => {
                    console.log('-', res.data)
                    commit('setReviewsList', res.data);
                })
                .catch(err => {
                    commit('setReviewsList', []);
                })
        },
        getChangeStatus({
            commit
        }, data) {
            return axios({
                method: 'put',
                url: apiURL + '/api/admin/review/' + data.id,
                headers: {
                    Authorization: 'Bearer ' + data.token
                },
                data: {
                    status: data.status
                }
            })
                .then(res => {
                    commit('setReviewEdit', res.data);
                })
        },
        deleteReview({
            commit
        }, data) {
            return axios({
                method: 'delete',
                url: apiURL + '/api/admin/review/' + data.id,
                headers: {
                    Authorization: 'Bearer ' + data.token
                }
            })
                .then(() => {
                    commit('setDeleteReview', data.id);
                })
        },
        getReviewFilter({
            commit
        }, token) {
            return axios({
                method: 'get',
                url: apiURL + '/api/admin/review-filter',
                headers: {
                    Authorization: 'Bearer ' + token
                }
            })
                .then(res => {
                    commit('setReviewFilter', res.data);
                })
        },
        getAnswerReview({
            commit
        }, data) {
            return axios({
                method: 'put',
                url: `${apiURL}/api/admin/review/${data.id}/answer`,
                headers: {
                    Authorization: 'Bearer ' + data.token
                },
                data: {
                    answer: data.answer
                }
            })
                .then(res => {
                    commit('setReviewEdit', res.data);
                })
        },
        getOrderDetails({
            commit
        }, data) {
            return axios({
                method: 'get',
                url: apiURL + '/api/order/' + data.orderId,
                headers: {
                    Authorization: 'Bearer ' + data.token,
                },
            })
                .then(response => {
                    commit('setOrderDetails', response.data);
                })
                .catch(error => {

                });
        }
    }
});

export default store;
