/*import axios from 'axios'
import {apiURL} from "./helpers/config";*/

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/img/favicons/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/static/plugins/font-awesome-4.7.0/css/font-awesome.min.css',
    '@/static/plugins/bulma.min.css',
    'swiper/dist/css/swiper.css',
    '@/assets/styles/plugins.scss',
    '@/assets/styles/stylesheet.scss',
    '@/assets/styles/main.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // '~/plugins/vue-awesome-swiper.js'
    // { src: '~/plugins/fullpage', mode: 'client' },
    { src: '~plugins/vue-fullpage.js', mode: 'client', ssr: false },
    { src: '~plugins/vue-awesome-swiper.js', ssr: false },
    { src: '~plugins/vue-cookie.js', ssr: false },
    { src: '~plugins/vue-editor.js', mode: 'client', ssr: false },
    { src: '~plugins/vue-tidio.js',mode:'client', ssr: false },
    // { src: '~plugins/vue-gtm.js', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // 'nuxt-fullpage.js',
    'fullpage-nuxt',
    ['@nuxtjs/html-minifier', { 
      collapseBooleanAttributes: true,
      decodeEntities: true,
      minifyCSS: true,
      minifyJS: true,
      processConditionalComments: true,
      removeEmptyAttributes: true,
      removeRedundantAttributes: true,
      trimCustomFragments: true,
      useShortDoctype: true }],
    ['@nuxtjs/google-tag-manager', { 
      id: 'GTM-PLQ86DG', // Your GTM single container ID or array of container ids ['GTM-xxxxxxx', 'GTM-yyyyyyy']
      queryParams: { // Add url query string when load gtm.js with GTM ID (optional)
        gtm_auth:'TuNFsvFQcU5sf7uGCEcGvg-MnOP8qr',
        gtm_preview:'env-4',
        gtm_cookies_win:'x'
      },
      enabled: true, // defaults to true. Plugin can be disabled by setting this to false for Ex: enabled: !!GDPR_Cookie (optional)
      debug: true, // Whether or not display console logs debugs (optional)
      loadScript: true 
    }],
    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '312124126008649',
      disabled: false
    }],
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  generate: {
    routes: [
      '/blog/8-ways-to-prevent-the-cold-and-flu-naturally',
      '/blog/coronavirus-scare!-how-to-boost-immunity'
    ],
    /* routes: async () => {
      const routes = []
      await axios({
        method: 'get',
        url: `https://cors-anywhere.herokuapp.com/${apiURL}/api/public/blog-list`,
      })
        .then(res => {
          routes = res.data.map((item) => ({
            route: `/blog/${item.article_url}`,
            payload: item
          }))
        })
        .catch(err => {
          console.log('generate', err)
        })

      return routes
    }, */
    minify: {
      collapseWhitespace: false
    }
  }
}
